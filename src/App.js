import React from "react";
import "./App.css";
import Navbar from "./component/navbar";
import Alert from "./component/alert";
import TextForm from "./component/textForm";
// import About from "./component/about";
// import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
function App() {
  let [mode, setMode] = React.useState("light");
  let [alert, setAlert] = React.useState(null);

  let showAlert = (message, type) => {
    setAlert({
      msg: message,
      type: type,
    });
    setTimeout(() => {
      setAlert(null);
    }, 1000);
  };

  let toggleMode = function () {
    if (mode == "light") {
      setMode("dark");
      document.body.style.backgroundColor = "161618";
      showAlert("Dark Mode Has been enables", "Success");
    } else {
      setMode("light");
      document.body.style.backgroundColor = "white";
      showAlert("Light Mode Has been enables", "Success");
    }
  };
  return (
    <>
      {/* <Router> */}
      <Navbar mode={mode} toggleMode={toggleMode} />
      <Alert alert={alert} />
      <div
        style={{
          // height: "100%",
          // paddingTop: "10px",
          backgroundColor: mode === "light" ? "white" : "#161618",
          color: mode === "light" ? "black" : "white",
        }}
      >
        {/* <Routes> */}
        {/* <Route path="/about" element={<About />} /> */}
        {/* <Route */}
        {/* path="/"
              element={ */}
        <TextForm
          showAlert={showAlert}
          header="Enter The Text To Analyze"
          mode={mode}
        />
        {/* }
            />
          </Routes> */}
        {/* <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/">
              <TextForm />
            </Route>
          </Switch> */}
      </div>
      {/* </Router> */}
    </>
  );
}

export default App;
