import React from "react";
let About = function () {
  return (
    <>
      <h3>About</h3>
      <p>
        We provide free tools to help you with your daily tasks. You will find
        tools for formatting source code, converters, tools for handling text,
        such as remove duplicate characters, empty lines, text sorter and many
        others. Check the current features below and feel free to recommend a
        new feature by contacting us.
      </p>
    </>
  );
};
export default About;
