import React from "react";

export default function TextForm(props) {
  let handleUpperCase = () => {
    let newText = text.toUpperCase();
    setText(newText);
    props.showAlert("Converted To Upper Case", "Success");
  };

  let handleOnchange = (event) => {
    setText(event.target.value);
  };

  let handleLowerCase = () => {
    let lowerCase = text.toLowerCase();
    setText(lowerCase);
    props.showAlert("Converted To Lower Case", "Success");
  };

  let handleClearText = () => {
    setText("");
    props.showAlert("Converted To Clear Text", "Success");
  };

  let handleCopyText = () => {
    let Text = document.getElementById("myBox");
    Text.select();
    navigator.clipboard.writeText(Text.value); ///???
    document.getSelection().removeAllRanges();
    props.showAlert("Copy Text", "Success");
  };

  let handleExtraSpaces = () => {
    let removeText = text.split(/[ ]+/); ////??? How
    setText(removeText.join(" "));
    props.showAlert("Remove extra space", "Success");
  };
  let [text, setText] = React.useState("");
  return (
    <>
      <div className="container">
        <h3>{props.header}</h3>
        <div className="mb-3">
          <textarea
            style={{
              backgroundColor: props.mode === "light" ? "white" : "#161618",
              color: props.mode === "light" ? "black" : "white",
              borderColor: props.mode === "light" ? "" : "#fffff",
            }}
            className="form-control"
            value={text}
            id="myBox"
            rows="12"
            onChange={handleOnchange}
          ></textarea>
        </div>
        <button
          disabled={text.length === 0}
          className="btn btn-primary  mx-1 my-1"
          onClick={handleUpperCase}
        >
          Convert To Uppercase
        </button>
        <button
          disabled={text.length === 0}
          className="btn btn-primary mx-1 my-1"
          onClick={handleLowerCase}
        >
          Convert To LowerCase
        </button>
        <button
          disabled={text.length === 0}
          className="btn btn-primary mx-1 my-1"
          onClick={handleExtraSpaces}
        >
          Remove Extra Spaces
        </button>
        <button
          disabled={text.length === 0}
          className="btn btn-primary mx-1 my-1"
          onClick={handleCopyText}
        >
          Copy Text
        </button>
        <button
          disabled={text.length === 0}
          className="btn btn-danger mx-1 my-1"
          onClick={handleClearText}
        >
          Clear Text
        </button>
      </div>
      <div className="container my-3">
        <h3>Your Text Summary</h3>
        <p>
          {
            text.split(" ").filter((element) => {
              return element != 0;
            }).length
          }{" "}
          Words and {text.length}
          characters
        </p>
        <p>
          {0.008 *
            text.split(" ").filter((element) => {
              return element != 0;
            }).length}{" "}
          Minutes Read
        </p>
        <h3>Preview</h3>
        <p>{text.length > 0 ? text : "Nothing To Preview"}</p>
      </div>
    </>
  );
}
